var events = require('events');

var event = new events.EventEmitter();

var device1 = {
    deviceId: 1,
    name: "Test",
    command: "ON",
    value: "ON"
};

module.exports.getDevices = function() {
	return [device1];
}

module.exports.on = function(listener, param) {
	event.on(listener, param);
}

module.exports.turnOn = function(id) {
	tellnode.turnOn(id);
}

module.exports.turnOff = function(id) {
	tellnode.turnOff(id);
}

module.exports.getSensors = function() {
	return [];
}
